import logging
import datetime
import md5
import mobplat
import vdc

class Meeting:
  def __init__(self, start_date, duration, people, rooms):
    self._start_date = start_date
    self._duration = duration
    self._invited = people
    self._checked = {}
    self._rooms = rooms

  def checkin(self, user_id, room_id):
    self._checked[user_id] = {'room_id': room_id, 'ts': datetime.datetime.now()}

class Room:
  checked = {}

  @classmethod
  def get_instance(cls, room_id):
    room = cls.rooms.get(room_id)
    if room is None:
      raise cls.RoomNotFoundError()
    return room

  def __init__(self, id, name ):
    self._id = id
    self._name = name
    self._checked = {}

  def checkin(user_id):
    self.checked[user_id] = {'ts': datetime.datetime.now()}

  def checkout(user_id):
    if self.checked.get(user_id):
      del self.checked[user_id]

  def as_dict(self):
    return {'id': self._id, 'name': self._name, 'cheched': self._checked}

  class RoomNotFoundError(Exception):
    def __init__(self):
      pass

class MeetingManager:
  sessions = {}
  meetings = {}

  room_vdc_mapping_for_demo = {'1D12': '0H19', '1D13': '0H26'}
  vdc_room_mapping_for_demo = {'0H19': '1D12', '0H26': '1D13'}

  MEETING_EARLY_START_TIME = 900 #900" = 15'
  MEETING_ROOM_BOOKING_MANDATORY = False

  @classmethod
  def login(cls, user_id, domain, password):
    mobcli = mobplat.MobilePlatClient(mobplat.MobilePlatClient.URL_PROD)
    mobcli.login(user_id, domain, password)
    cls.sessions[mobcli.session_id] = mobcli
    return mobcli.session_id

  @classmethod
  def check(cls, session_id):
    mobcli = cls.sessions[session_id]
    return mobcli.check_token()

  @classmethod
  def get_meeting(cls, session_id, meeting_id):
    mobcli = cls.sessions[session_id]
    meeting = cls.meetings.get(meeting_id)
    if meeting is None:
      meeting = mobcli.get_meeting(meeting_id=meeting_id)
      cls.meetings[meeting_id] = meeting
    return meeting

  @classmethod
  def get_meeting_id(cls, meeting):
    return md5.new(meeting.get("subject") + "-" + meeting.get("gmtStartDate") + "-" + meeting.get("gmtEndDate")).hexdigest()

  @classmethod
  def checkin(cls, session_id, user_id, room_id):
    mobcli = cls.sessions[session_id]
    now = datetime.datetime.now()
    today = datetime.date.today()
    room_id = cls.vdc_room_mapping_for_demo.get(room_id)
    meetings = mobcli.get_meeting_list(start_date=today, num_days=1)
    meeting = cls.find_meeting_by_ts_room_id(meetings=meetings, ts=now, room_id=room_id)
    if meeting:
      if cls.meetings.get(cls.get_meeting_id(meeting)):
        meeting = cls.meetings.get(cls.get_meeting_id(meeting))
      else:
        meeting["uid"] = cls.get_meeting_id(meeting)
        cls.meetings[cls.get_meeting_id(meeting)] = meeting
      cls.checkin_room(meeting, user_id, room_id)
      cls.connect_rooms(meeting)
    return meeting

  @classmethod
  def checkout(cls, session_id, user_id, meeting_id):
    mobcli = cls.sessions[session_id]
    meeting = cls.meetings.get(meeting_id)
    if meeting:
      try:
        meeting.get("checked").remove(user_id)
      except:
        logging.error("user_id_not_found")
      for room in meeting.get("rooms"):
        try:
          room.remove(user_id)
        except:
          logging.error("user_id_not_found")
    return meeting

  @classmethod
  def find_meeting_by_ts_room_id(cls, meetings, ts, room_id):
    meeting = None
    for m in meetings:
      m_start = datetime.datetime.fromtimestamp((int(m.get('gmtStartDate'))/1000)-cls.MEETING_EARLY_START_TIME)
      m_end = datetime.datetime.fromtimestamp(int(m.get('gmtEndDate'))/1000)
      if m_start < ts and ts < m_end:
        m_rooms = m.get('rooms')
        # check if rooms booking is mandatory
        if cls.MEETING_ROOM_BOOKING_MANDATORY:
          #if yes, then check room_id in meeting.rooms
          if cls.get_room_by_id(m_rooms, room_id):
            meeting = m
            break
        else:
          #else take the first meeting, init rooms with
          meeting = m
          break

    return meeting

  @classmethod
  def get_room_by_id(cls, rooms, room_id):
    room = None
    for r in rooms:
      r_id = str(r.get("name").split()[0])
      if r_id == room_id:
        room = r
        break
    return room

  @classmethod
  def checkin_room(cls, meeting, user_id, room_id):
    if meeting.get('checked') == None:
      meeting['checked'] = []
    try:
      meeting['checked'].remove(user_id)
    except:
      logging.info("user not present")

    meeting['checked'].append(user_id)

    room = cls.get_room_by_id(meeting.get('rooms'), room_id)
    if room is None:
      room = {"name":room_id, "checked": []}
      meeting["rooms"].append(room)
    try:
      room['checked'].remove(user_id)
    except:
      logging.info("user not present")
    room['checked'].append(user_id)
    logging.info("meeting.subj: " + meeting.get("subject") + " checkin - user_id: " + user_id + " room: " + str(room_id))

  @classmethod
  def connect_rooms(cls, meeting):
    rooms = meeting.get('rooms')
    calling_room = cls.find_primary_room(rooms)
    if calling_room:
      calling_room_name = cls.room_vdc_mapping_for_demo.get(calling_room.get("name").split()[0])
      device = vdc.CiscoC40.get_instance(calling_room_name)
      for room in rooms:
        if calling_room != room and len(room.get('checked', [])) > 0 and (room.get('connected', False) == False or device.get_call_status(room.get('call_id') != device.CALL_STATUS_CONNECTED)):
          dest_room_name = room.get("name").split()[0]
          dest_room_addr = cls.room_vdc_mapping_for_demo.get(dest_room_name)
          call_id = device.dial(dest_room_addr)
          calling_room['connected'] = True
          room['connected'] = True
          room['call_id'] = call_id
          logging.info("connected src: " + calling_room_name + " dest: " + dest_room_addr + " call_id: " + str(call_id))

  @classmethod
  def find_primary_room(cls, rooms):
    #first see if room aleady selected
    room = None
    for r in rooms:
      if r.get('primary', False) == True:
        room = r
    #no room selected as primary yet, take the first one which has a checkin
    if room is None and len(rooms):
      for r in rooms:
        if len(r.get('checked', [])) > 0 and vdc.CiscoC40.is_device_managed(cls.room_vdc_mapping_for_demo.get(r.get("name").split()[0])):
          room = r
          room['primary'] = True

    return room
