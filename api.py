import io
import json
import logging
import urllib2
import vdc
import mobplat
import meeting

from flask import Flask, request
from flask.ext.cors import CORS

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
  return "api server v1.0"

@app.route("/vdc/1.0/devices/list", methods=['GET'])
def device_list():
  logging.info("device_list.1")
  device_list = vdc.CiscoC40.get_device_list()
  ret_val = {"status": "ok", 'device_list': device_list}
  return json.dumps(ret_val)

@app.route("/vdc/1.0/devices/<vdc_id>", methods=['GET'])
def device_status(vdc_id):
  logging.info("device_status.1")
  ret_val = {}
  input = request.get_data()
  data = json.loads(input)
  device = vdc.CiscoC40.get_instance(vdc_id)
  if device:
     values = device.status(data.get('path'))
     ret_val = {"status": "ok", 'values': values }
  else:
     ret_val = {"status": "ko", 'error': 'device_not_found' }
  return json.dumps(ret_val)

@app.route("/vdc/1.0/devices/<vdc_id>/call", methods=['POST'])
def device_call(vdc_id):
  logging.info("device_call.1")
  ret_val = {}
  input = request.get_data()
  data = json.loads(input)
  device = vdc.CiscoC40.get_instance(vdc_id)
  if vdc:
    call_id = device.dial(data.get('dest'))
    ret_val = {"status": "ok", 'call_id': call_id }
  else:
     ret_val = {"status": "ko", 'error': 'device_not_found' }
  return json.dumps(ret_val)

@app.route("/vdc/1.0/devices/<vdc_id>/call/<call_id>", methods=['DELETE'])
def devices_disconnect(vdc_id, call_id):
  logging.info("devices_disconnect.1")
  ret_val = {}
  device = vdc.CiscoC40.get_instance(vdc_id)
  if device:
    status = device.disconnect(call_id)
    ret_val = {"status": status }
  else:
     ret_val = {"status": "ko", 'error': 'device_not_found' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/rooms/<room_id>", methods=['GET'])
def room_status(room_id):
  logging.info("room_status.1")
  ret_val = {}
  r = room.Room.get_instance(room_id)
  if r:
    meeting_id = r.checkin(data.get('user_id'))
    ret_val = {"status": "ok", 'meeting_data': r.as_dict() }
  else:
     ret_val = {"status": "ko", 'error': 'room_not_found' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/rooms/<room_id>/checkin", methods=['POST'])
def room_checkin(room_id):
  logging.info("room_checkin.1")
  ret_val = {}
  input = request.get_data()
  data = json.loads(input)
  r = room.Room.get_instance(room_id)
  if room:
    meeting_id = r.checkin(data.get('user_id'))
    ret_val = {"status": "ok", 'meeting_id': meeting_id }
  else:
     ret_val = {"status": "ko", 'error': 'room_not_found' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/rooms/<room_id>/checkout", methods=['POST'])
def room_checkout(room_id):
  logging.info("room_checkout.1")
  ret_val = {}
  input = request.get_data()
  data = json.loads(input)
  r = room.Room.get_instance(room_id)
  if room:
    meeting_id = r.checkout(data.get('user_id'))
    ret_val = {"status": "ok", 'meeting_id': meeting_id }
  else:
     ret_val = {"status": "ko", 'error': 'room_not_found' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/auth/login", methods=['POST'])
def auth_login():
  logging.info("auth_login.1")
  ret_val = {}
  try:
    input = request.get_data()
    data = json.loads(input)
    session_id = meeting.MeetingManager.login(user_id=data['user_id'], domain=data['domain'], password=data['password'])
    if session_id:
      ret_val = {"status": "ok", 'session_id': session_id }
    else:
      raise Exception
  except:
    ret_val = {"status": "ko", 'error': 'login_failed' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/auth/check", methods=['POST'])
def auth_check():
  logging.info("auth_login.1")
  ret_val = {}
  session_id = request.headers["Authorization"]
  try:
    status = meeting.MeetingManager.check(session_id=session_id)
    ret_val = {"status": "ok", 'status': status }
  except:
    ret_val = {"status": "ko", 'error': 'token_expired' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/meeting/<meeting_id>", methods=['GET'])
def meeting_status(meeting_id):
  logging.info("meeting_status.1")
  ret_val = {}
  try:
    session_id = request.headers["Authorization"]
    m = meeting.MeetingManager.get_meeting(session_id=session_id, meeting_id=meeting_id)
    if m:
      ret_val = {"status": "ok", 'meeting_data': m }
    else:
      ret_val = {"status": "ko", 'error': 'meeting_not_found' }
  except Exception as e:
    logging.error("Exception: " + str(e))
    ret_val = {"status": "ko", 'error': 'invalid_request' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/meeting/checkin", methods=['POST'])
def meeting_checkin():
  logging.info("meeting_checkin.1")
  ret_val = {}
  try:
    session_id = request.headers["Authorization"]
    input = request.get_data()
    data = json.loads(input)
    m = meeting.MeetingManager.checkin(session_id=session_id, user_id = data.get('user_id'), room_id = data.get('room_id'))
    if m:
      ret_val = {"status": "ok", 'meeting_data': m }
    else:
      ret_val = {"status": "ko", 'error': 'meeting_not_found' }
  except Exception as e:
    logging.error("Exception: " + str(e))
    ret_val = {"status": "ko", 'error': 'invalid_request' }
  return json.dumps(ret_val)

@app.route("/meeting/1.0/meeting/checkout", methods=['POST'])
def meeting_checkout():
  logging.info("meeting_list.1")
  ret_val = {}
  try:
    session_id = request.headers["Authorization"]
    input = request.get_data()
    data = json.loads(input)
    m = meeting.MeetingManager.checkout(session_id=session_id, user_id = data.get('user_id'), meeting_id = data.get('meeting_id'))
    if m:
      ret_val = {"status": "ok", 'meeting_data': m }
    else:
      ret_val = {"status": "ko", 'error': 'meeting_not_found' }
  except Exception as e:
    logging.info("Exception: " + str(e))
    ret_val = {"status": "ko", 'error': 'invalid_request' }
  return json.dumps(ret_val)

@app.route("/test")
def test():
  return app.send_static_file('test.html')

if __name__ == "__main__":
  app.run(host='0.0.0.0',port=5000)
