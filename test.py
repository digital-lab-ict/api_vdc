#!/usr/bin/python
import unittest
import sys
import mobplat
import meeting
import vdc
import datetime
import time
import logging

logging.basicConfig(level=logging.DEBUG)

user1 = None
domain1 = None
password1 = None
room1 = None
user2 = None
domain2 = None
password2 = None
room2 = None

class TestMobilePlat(unittest.TestCase):
  def test_login(self):
    mobcli = mobplat.MobilePlatClient(mobplat.MobilePlatClient.URL_PROD)
    self.assertFalse(mobcli is None)

    session_id = mobcli.login(user_id=user1, domain=domain1, password=password1)
    self.assertFalse(session_id is None)

    resp = mobcli.check_token()
    self.assertTrue(resp)

  def test_meeting_list(self):
    mobcli = mobplat.MobilePlatClient(mobplat.MobilePlatClient.URL_PROD)
    self.assertFalse(mobcli is None)

    session_id = mobcli.login(user_id=user1, domain=domain1, password=password1)
    self.assertFalse(session_id is None)

    meetings = mobcli.get_meeting_list(start_date=datetime.date.today(), num_days=10)
    self.assertFalse(meetings is None)

class TestMeetingManager(unittest.TestCase):
  def test_login(self):
    session_id = meeting.MeetingManager.login(user_id=user1, domain=domain1, password=password1)
    self.assertFalse(session_id is None)

    status = meeting.MeetingManager.check(session_id=session_id)
    self.assertTrue(status)

  def test_checkin(self):
    session_id = meeting.MeetingManager.login(user_id=user1, domain=domain1, password=password1)
    self.assertFalse(session_id is None)

    m = meeting.MeetingManager.checkin(session_id, user_id=user1, room_id=room1)
    self.assertFalse(m is None)
    #logging.info("meeting_user_1: " + str(m))

    session_id = meeting.MeetingManager.login(user_id=user2, domain=domain2, password=password2)
    self.assertFalse(session_id is None)

    m = meeting.MeetingManager.checkin(session_id, user_id=user2, room_id=room2)
    self.assertFalse(m is None)
    #logging.info("meeting_user_2: " + str(m))

    time.sleep(5)
    device = vdc.CiscoC40.get_instance('0H19').disconnect_all()
    time.sleep(5)

    m = meeting.MeetingManager.checkin(session_id, user_id=user1, room_id=room1)
    self.assertFalse(m is None)

  def test_checkout(self):
    session_id = meeting.MeetingManager.login(user_id=user1, domain=domain1, password=password1)
    self.assertFalse(session_id is None)

    m = meeting.MeetingManager.checkin(session_id=session_id, user_id=user1, room_id=room1)
    self.assertFalse(m is None)

    m = meeting.MeetingManager.get_meeting(session_id=session_id, meeting_id=m.get("uid"))
    self.assertFalse(m is None)

    m = meeting.MeetingManager.checkout(session_id=session_id, user_id=user1, meeting_id=m.get("uid"))
    self.assertFalse(m is None)

if __name__ == '__main__':
  if len(sys.argv) < 8:
    print "usage: " + sys.argv[0] + " user_id domain password room1 room2"
    exit()
  user1 = sys.argv[1]
  domain1 = sys.argv[2]
  password1 = sys.argv[3]
  room1 = sys.argv[4]
  user2 = sys.argv[5]
  domain2 = sys.argv[6]
  password2 = sys.argv[7]
  room2 = sys.argv[8]
  sys.argv = sys.argv[:1]
  unittest.main()
