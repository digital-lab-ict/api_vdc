import urllib2
import base64
import md5
import json
import datetime
from xml.dom.minidom import parse
import logging

class MobilePlatClient:
  PWD_CRYPTKEY = '!2014eni'
  URL_PROD = 'https://mobileplat.eni.it/EniServiceRooms/EniService.svc/services'
  URL_TEST = 'https://st-mobileplat.eni.it/EniServiceRooms/EniService.svc/services'

  def __init__(self, url):
    self._url = url
    self._session = None
    self._contacts = []

  @property
  def session_id(self):
    return self._session.token

  @property
  def contacts(self):
    return self._contacts

  def login(self, user_id, domain, password):
    command = {'username': user_id, 'domain': domain}
    command['password'] = self.encode_password(password)
    data = json.dumps(command)
    req = urllib2.Request(self._url + '/login', data)
    req.add_header('Content-Type', 'application/json')
    req.add_header('Accept', 'application/json')
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      resp = json.load(handle)
      self._session = self.Session(user_id, resp.get('token'))
      self._contacts = resp.get('contacts')
      return self._session
    except IOError, e:
      logging.error(str(e))
      if hasattr(e, 'code'):
        raise self.MBCError(e.code, str(e))
      else:
        raise

  def check_token(self):
    req = urllib2.Request(self._url + '/checkToken', "")
    req.add_header("Authorization", self._session.user_id + ':' + self._session.token)
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      return True
    except IOError, e:
      logging.error(str(e))
      if hasattr(e, 'code'):
        raise self.MBCError(e.code, str(e))
      else:
        raise

  # GET /services/meeting/list/{yyyyMmDd}/{numDays}
  def get_meeting_list(self, start_date, num_days=1):
    req = urllib2.Request(self._url + '/meeting/list/' + start_date.strftime('%Y%m%d') + '/' + str(num_days))
    req.add_header("Authorization", self._session.user_id + ':' + self._session.token)

    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      resp = json.load(handle)
      return resp
    except IOError, e:
      logging.error(str(e))
      if hasattr(e, 'code'):
        raise self.MBCError(e.code, str(e))
      else:
        raise

  def get_meeting(self, meeting_id):
    data = json.dumps({"id":meeting_id})
    req = urllib2.Request(self._url + '/meeting/detail', data)
    req.add_header("Authorization", self._session.user_id + ':' + self._session.token)
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      resp = json.load(handle)
      return resp
    except IOError, e:
      logging.error(str(e))
      if hasattr(e, 'code'):
        raise self.MBCError(e.code, str(e))
      else:
        raise

  def update_meeting(self, meeting_data):
    data = json.dumps(meeting_data)
    req = urllib2.Request(self._url + '/meeting', data)
    req._method = 'PUT'
    req.add_header("Authorization", self._session.user_id + ':' + self._session.token)
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      resp = json.load(handle)
      return resp
    except IOError, e:
      logging.error(str(e))
      if hasattr(e, 'code'):
        raise self.MBCError(e.code, str(e))
      else:
        raise


  class Session:
    def __init__(self, user_id, token):
      self._user_id = user_id
      self._token = token

    @property
    def user_id(self):
      return self._user_id

    @property
    def token(self):
      return self._token

  class MBCError(Exception):
    def __init__(self, code, message):
      self.code = code
      self.message = message

  @classmethod
  def encode_password(cls, pwd, key=PWD_CRYPTKEY):
    enc_pwd = ''
    for i in range(0, len(pwd)):
      e = "" + str(ord(pwd[i]) ^ ord(key[i % len(key)]))
      while len(e) < 3:
        e = '0' + e
      enc_pwd += e
    return enc_pwd
