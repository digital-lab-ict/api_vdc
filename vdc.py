import urllib2
import base64
from xml.dom.minidom import parse
import logging

class CiscoC40:
  CISCO_C40_GET_URL = '/getxml?location='
  CISCO_C40_PUT_URL = '/putxml'
  CMD_DIAL = '<Command><Dial command="True"><Number>{dest_addr}</Number><Protocol>{protocol}</Protocol></Dial></Command>'
  CMD_ACCEPT = '<Command><Call command="True"><Accept><CallId>{call_id}</CallId></Accept></Call></Command>'
  CMD_DISCONNECT = '<Command><Call command="True"><Disconnect><CallId>{call_id}</CallId></Disconnect></Call></Command>'
  CMD_DISCONNECT_ALL = '<Command><Call command="True"><DisconnectAll></DisconnectAll></Call></Command>'
  CMD_STANDBY = '<Command><Standby command="True"><Activate></Activate></Standby></Command>'

  PATH_CALL_STATUS = '/Status/Call[contains(@item,{call_id})]'

  CALL_STATUS_CONNECTED = 'Connected'

  PROTOCOL_H323 = 'H323'
  PROTOCOL_SIP = 'SIP'

  devices = {
    '0H19': {'ip':'10.130.221.100', 'protocol': 'H323', 'id': '7520357@video.eni.it', 'user': 'admin', 'pwd': ''},
    '0H26': {'ip':'', 'protocol': 'H323', 'id': '7520086@video.eni.it', 'user': None, 'pwd': None},
    '1D12': {'ip':'', 'protocol': 'H323', 'id': '7520277@video.eni.it', 'user': None, 'pwd': None},
    '1D13': {'ip':'', 'protocol': 'H323', 'id': '7520228@video.eni.it', 'user': None, 'pwd': None},
  }

  @classmethod
  def get_instance(cls, name):
    device = None
    dev_conf = cls.devices.get(name)
    if dev_conf:
      device = CiscoC40(host=dev_conf['ip'], name=name, user=dev_conf['user'], pwd=dev_conf['pwd'])
    return device

  @classmethod
  def get_device_list(cls):
    return cls.devices.keys()

  @classmethod
  def get_device_info_by_name(cls, name):
    return cls.devices.get(name)

  @classmethod
  def is_device_managed(cls, name):
    return cls.devices.get(name).get('user') is not None

  def __init__(self, host, name, user, pwd):
    self._host = host
    self._name = name
    self._user = user
    self._pwd = pwd

  def dial(self, dest_name):
    dest_conf = self.get_device_info_by_name(dest_name)
    resp = self.command(command=self.CMD_DIAL, dest_addr=dest_conf['id'], protocol=dest_conf['protocol'])
    status = resp.getElementsByTagName('DialResult')[0].getAttribute('status')
    if status != "OK":
      raise IOError("Device error: " + status)
    return resp.getElementsByTagName('CallId')[0].firstChild.data

  def dial_addr(self, dest_addr, protocol=PROTOCOL_SIP):
    resp = self.command(command=self.CMD_DIAL, dest_addr=dest_addr, protocol=protocol)
    status = resp.getElementsByTagName('DialResult')[0].getAttribute('status')
    if status != "OK":
      raise IOError("Device error: " + status)
    return resp.getElementsByTagName('CallId')[0].firstChild.data

  def disconnect(self, call_id):
    resp = self.command(command=self.CMD_DISCONNECT, call_id=call_id)
    status = resp.getElementsByTagName('DisconnectCallResult')[0].getAttribute('status')
    if status != "OK":
      raise IOError("Device error: " + status)
    return status.lower()

  def disconnect_all(self):
    resp = self.command(command=self.CMD_DISCONNECT_ALL)
    status = resp.getElementsByTagName('DisconnectAllResult')[0].getAttribute('status')
    if status != "OK":
      raise IOError("Device error: " + status)
    return status.lower()

  def standby(self):
    resp = self.command(command=self.CMD_STANDBY)
    status = resp.getElementsByTagName('StandbyResult')[0].getAttribute('status')
    if status != "OK":
      raise IOError("Device error: " + status)
    return status.lower()

  def get_call_status(self, call_id):
    path = self.PATH_CALL_STATUS.format(call_id=call_id)
    resp = self.status(path)
    if resp.getElementsByTagName('Status'):
      return resp.getElementsByTagName('Status')[0].getElementsByTagName("Call")[0].getElementsByTagName("Status")[0].firstChild.data
    else:
      return "Disconnected"

  def status(self, path):
    req = urllib2.Request('http://' + self._host + self.CISCO_C40_GET_URL + path)
    return self.http_send(req)

  def command(self, command, **kwargs):
    command = command.format(**kwargs)
    req = urllib2.Request('http://' + self._host + self.CISCO_C40_PUT_URL, command)
    return self.http_send(req)

  def http_send(self, req):
    base64string = base64.encodestring('%s:%s' % (self._user, self._pwd))[:-1]
    req.add_header("Authorization", "Basic %s" % base64string)
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      resp = parse(handle)
      #logging.info("resp: " + str(resp.toprettyxml()))
      return resp
    except IOError, e:
      if hasattr(e, 'code'):
        logging.error("IOError: " + str(e.code) + " message: " + str(e))
        raise self.VDCError(e.code, str(e))
      else:
        raise

  class VDCError(IOError):
    def __init__(self, code, desc):
      self.code = code
      self.desc = desc

if __name__ == "__main__":
  try:
    vdc = CiscoC40.get_instance('0H19')
    ret = vdc.status('/Status/Audio')
    print ret
  except CiscoC40.VDCError as e:
    logging.error("Error: " + str(e.code) + " desc: " + str(e.desc))
